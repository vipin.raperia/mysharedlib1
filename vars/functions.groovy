def call(){
    script{
        checkout scm
        config = readProperties file: 'Configuration'
        git branch:"${config.git_branch}", url:"${config.git_url}"
    }
}
